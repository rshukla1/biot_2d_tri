#include "dfloat.h"
#include "fem.h"
//#include "types.h"

// may want to generalize later to multiple devices...

// sets up mesh-based occa parameters
void setupOccaMesh2d(Mesh *mesh, App *app){

 // app->props = occa::getKernelProperties(); //props;

  app->props["defines/p_Np"] = mesh->Np; // number of dims
  app->props["defines/p_Np1"] = mesh->N + 1;  // (N+1)
  app->props["defines/p_Np2"] = (mesh->N + 1)*(mesh->N + 1);  // (N+1)  

  app->props["defines/p_Nq1"] = mesh->N + 1;  // (N+1)
  app->props["defines/p_Nq2"] = (mesh->N + 1)*(mesh->N + 1);  
  //  app->props["defines/p_Nq3"] = (mesh->N + 1)*(mesh->N + 1)*(mesh->N + 1);  
  
  app->props["defines/p_Nfaces"] = mesh->Nfaces;
  app->props["defines/p_Nfp"] = mesh->Nfp;
  app->props["defines/p_NfpNfaces"] = mesh->Nfp * mesh->Nfaces;
  app->props["defines/p_T"] = max(mesh->Nfp * mesh->Nfaces,mesh->Np);

  int Nvgeo = 5; // rs/xy (2 x 2) +J
  int Nfgeo = 3; //Number of edges or faces
  app->props["defines/p_Nvgeo"] = Nvgeo;
  app->props["defines/p_Nfgeo"] = Nfgeo;
  app->props["defines/p_Nq_reduced"]=mesh->Vq.rows();
  app->props["defines/p_tau_v"]=1.0;
  app->props["defines/p_tau_s"]=1.0;
  app->props["defines/p_KblkV"]=6;
  app->props["defines/p_KblkS"]=6;
  app->props["defines/p_KblkU"]=6;
  app->props["defines/p_KblkQ"]=6;
  app->props["defines/p_KblkQf"]=6;

  // switch dfloat type (double/float) in types.h
  if (sizeof(dfloat)==4){
    app->props["defines/USE_DOUBLE"] = 0;
  }else{
    app->props["defines/USE_DOUBLE"] = 1;
  }

  // allocate space for geofaces
  int K = mesh->K;
  int Np = mesh->Np;
  int NfpNfaces = (mesh->Nfp)*(mesh->Nfaces);  
  // interpolate to quad pts and store
  MatrixXd Vq   = mesh->Vq;
  MatrixXd Pq   = mesh->Pq;
  MatrixXd Dr   = mesh->Dr;
  MatrixXd Ds   = mesh->Ds;

    
  MatrixXd rxJq = (mesh->rxJ);
  MatrixXd sxJq = (mesh->sxJ);
  MatrixXd ryJq = (mesh->ryJ);
  MatrixXd syJq = (mesh->syJ);
  MatrixXd Jq   = (mesh->J); // interp: may lose some accuracy

  MatrixXd vgeo(Nvgeo*Np,K);  
  vgeo << rxJq,
    ryJq,
    sxJq,
    syJq,
    Jq;	  
  // interp to face values 
  MatrixXd Vf   = mesh->Vf;
  MatrixXd rxJf = Vf*(mesh->rxJ);
  MatrixXd sxJf = Vf*(mesh->sxJ);
  MatrixXd ryJf = Vf*(mesh->ryJ);
  MatrixXd syJf = Vf*(mesh->syJ);
  MatrixXd Jf   = Vf*(mesh->J); // this may not be accurate. I don't think I use though.
  MatrixXd vfgeo(Nvgeo*(Vf.rows()),K);
  vfgeo << rxJf,
    ryJf,
    sxJf,
    syJf,
    Jf;	  
 
  // Porous Property element of Matrix Q^-1
   
  MatrixXd LIFT = mesh->LIFT; 

   MatrixXd c11(Vq.rows(), mesh->K); //Vq.rows()=size of Cubature 2*N
   MatrixXd c33(Vq.rows(), mesh->K);
   MatrixXd c13(Vq.rows(), mesh->K);
   MatrixXd c12(Vq.rows(), mesh->K);  
   MatrixXd c55(Vq.rows(), mesh->K);
  
   MatrixXd a1(Vq.rows(), mesh->K);  
   MatrixXd a3(Vq.rows(), mesh->K);
   
   MatrixXd m1(Vq.rows(), mesh->K);
   MatrixXd m3(Vq.rows(), mesh->K);
   
   MatrixXd M(Vq.rows(), mesh->K);
   MatrixXd eta(Vq.rows(), mesh->K);

   MatrixXd kappa1(Vq.rows(), mesh->K);
   MatrixXd kappa3(Vq.rows(), mesh->K);
 
   MatrixXd rho(Vq.rows(), mesh->K);
   MatrixXd rhof(Vq.rows(), mesh->K);
   
   Mat *mat = new Mat;
   matProp(mat);
   c11.fill(mat->C11);
   c33.fill(mat->C33);
   c13.fill(mat->C13);
   c12.fill(mat->C12);
   c55.fill(mat->C55);
   
   a1.fill(mat->ALPHA1);   
   a3.fill(mat->ALPHA3);

   m1.fill(mat->m1);
   m3.fill(mat->m3);

   M.fill(mat->M);
   eta.fill(mat->ETA);

   kappa1.fill(mat->KAPPA1);
   kappa3.fill(mat->KAPPA3);
   rho.fill(mat->RHO);
   rhof.fill(mat->RHOF);

   setOccaArray(app, c11, app->o_c11);
   setOccaArray(app, c33, app->o_c33);
   setOccaArray(app, c13, app->o_c13);
   setOccaArray(app, c12, app->o_c12);
   setOccaArray(app, c55, app->o_c55);
   setOccaArray(app, a1, app->o_a1);
   setOccaArray(app, a3, app->o_a3);
   setOccaArray(app, m1, app->o_m1);
   setOccaArray(app, m3, app->o_m3);
   setOccaArray(app, M, app->o_M);
   setOccaArray(app, eta, app->o_eta);
   setOccaArray(app, kappa1, app->o_kappa1);
   setOccaArray(app, kappa3, app->o_kappa3);
   setOccaArray(app, rho, app->o_rho);
   setOccaArray(app, rhof, app->o_rhof);


   setOccaArray(app, LIFT, app->o_LIFT);


 // Ricker Source 

  MatrixXd fsrcq(Vq.rows(), mesh->K);
  double a=1;
  double x0=0.0;
  double y0=0.0;;


  for(int e=0; e < mesh->K; ++e){
	 VectorXd xq(Vq.rows()), yq(Vq.rows());
	 xq=Vq*(mesh->x.col(e));
         yq=Vq*(mesh->y.col(e));
	 for(int i=0; i < Vq.rows(); i++){
		 double dx=xq(i)-x0;
 		 double dy=yq(i)-y0;
		 double r2=dx*dx + dy*dy;
		 fsrcq(i,e)=exp(-a*a*r2);
	 }
}
  fsrcq *= 1.0/fsrcq.array().abs().maxCoeff();
  //cout << fsrcq << endl;

  MatrixXd fsrc =Pq * fsrcq;
 setOccaArray(app, fsrc, app->o_fsrcq);

   // normals computed at face quad points already
   MatrixXd fgeo(Nfgeo*NfpNfaces,K);
   fgeo << mesh->nxJ,
     mesh->nyJ,
     mesh->sJ;


  // init rhs, res, Qf to zero
  int Nfields = mesh->Nfields;
  MatrixXd rhs(Nfields*Np,K), res(Nfields*Np,K);
  rhs.fill(0.0);
  res.fill(0.0);
  MatrixXi Fmask = mesh-> Fmask;
  //MatrixXi Fmask = temp.transpose();
  setOccaIntArray(app, Fmask, app->o_Fmask);

  setOccaArray(app, rhs, app->o_rhs);
  setOccaArray(app, res, app->o_res);  
   
  
  setOccaArray(app, vgeo, app->o_vgeo);
  setOccaArray(app, vfgeo, app->o_vfgeo);  
  setOccaArray(app, fgeo, app->o_fgeo);
  setOccaArray(app, Vq, app-> o_Vq);
  setOccaArray(app, Pq, app-> o_Pq);
  setOccaArray(app, Dr, app-> o_Dr);
  setOccaArray(app, Ds, app-> o_Ds);

  VectorXi vmapP=mesh->vmapP;
  VectorXi vmapM=mesh->vmapM;
  VectorXi  mapP=mesh->mapP;
  VectorXi  mapM=mesh->vmapM;
  VectorXi h_vmapP;
  h_vmapP.resize(mesh->Nfp*mesh->Nfaces*mesh->K);
  h_vmapP.fill(0);



  for (int e=0; e < mesh->K; ++e){
      for(int i=0; i < mesh->Nfp*mesh->Nfaces; ++i){
	  int f=i/mesh->Nfp;
	  int idP=vmapP(i + mesh->Nfp*mesh->Nfaces*e);
	   int eNbr=mesh->EToE(e,f);
	   idP -= Np*eNbr;
	   idP += Np*Nfields*eNbr;
	   h_vmapP(i+mesh->Nfp*mesh->Nfaces*e) = idP;
	 
	 }
  }

    //cout << "hvmapP" << h_vmapP << endl;


  setOccaIntVector(app, h_vmapP, app-> o_vmapP);
  setOccaIntVector(app, mesh->vmapM,app-> o_vmapM);
  setOccaIntVector(app, mesh->mapP,app-> o_mapP);
  setOccaIntVector(app, mesh->mapM,app-> o_mapM);


 
  // correct for Nfields > 1
  MatrixXi mapPqNfields(NfpNfaces,K);
  for (int i = 0; i < NfpNfaces*K; ++i){
    int idP = mesh->mapPq(i);
    int e = idP / NfpNfaces;
    int fidP = idP % NfpNfaces;
    mapPqNfields(i) = fidP + e*NfpNfaces*Nfields;
  }
  //cout << "mapPq = " << mesh->mapPq << endl;
  //cout << "mapPqNfields = " << mapPqNfields << endl;
  setOccaIntArray(app, mapPqNfields, app->o_mapPq);
  
  //setOccaArray(app, mesh->D1D, app->o_D1D);  ???? do in need that
  //setOccaArray(app, mesh->Vf1D.row(0), app->o_Vf1D); // Vf1D rows 1,2 = mirror images
  //setOccaArray(app, mesh->Lf1D.col(0), app->o_Lf1D); // Lf1D cols 1,2 = mirror images  
  //cout << "Vf1D = " << mesh->Vf1D.row(0) << endl;
  //cout << "Lf1D = " << mesh->Lf1D.col(0) << endl;
}


// sets up mesh-based occa parameters
void setupOccaMesh3d(Mesh *mesh, App *app){

  //app->props = occa::getKernelProperties();

  app->props["defines/p_Np"] = mesh->Np; // number of dims

  app->props["defines/p_Nq1"] = mesh->N + 1;  // (N+1)
  app->props["defines/p_Nq2"] = (mesh->N + 1)*(mesh->N + 1);  
  app->props["defines/p_Nq3"] = (mesh->N + 1)*(mesh->N + 1)*(mesh->N + 1);  
    
  app->props["defines/p_Nfaces"] = mesh->Nfaces;
  app->props["defines/p_Nfp"] = mesh->Nfp;
  app->props["defines/p_Nfp2"] = 2*mesh->Nfp;  
  app->props["defines/p_NfpNfaces"] = mesh->Nfp * mesh->Nfaces;
  app->props["defines/p_T"] = max(mesh->Nfp * mesh->Nfaces,mesh->Np);

  int Nvgeo = 10;  // 3x3 geofacs + J
  int Nfgeo = 4; 
  app->props["defines/p_Nvgeo"] = Nvgeo;
  app->props["defines/p_Nfgeo"] = Nfgeo;
  
  // switch dfloat type (double/float) in types.h
  if (sizeof(dfloat)==4){
    app->props["defines/USE_DOUBLE"] = 0;
  }else{
    app->props["defines/USE_DOUBLE"] = 1;
  }

  // allocate space for geofaces
  int K = mesh->K;
  int Np = mesh->Np;
  int NfpNfaces = (mesh->Nfp)*(mesh->Nfaces);  

  // interpolate to quad pts and store
  MatrixXd Vq   = mesh->Vq;
  MatrixXd rxJq = Vq*(mesh->rxJ);
  MatrixXd sxJq = Vq*(mesh->sxJ);
  MatrixXd txJq = Vq*(mesh->txJ);  
  MatrixXd ryJq = Vq*(mesh->ryJ);
  MatrixXd syJq = Vq*(mesh->syJ);
  MatrixXd tyJq = Vq*(mesh->tyJ);  
  MatrixXd rzJq = Vq*(mesh->rzJ);
  MatrixXd szJq = Vq*(mesh->szJ);
  MatrixXd tzJq = Vq*(mesh->tzJ);  
  MatrixXd Jq   = Vq*(mesh->J); // interp: may lose some accuracy
  //cout << "Jq = " << Jq << endl;

  MatrixXd vgeo(Nvgeo*Vq.rows(),K);  
  vgeo << rxJq,
    ryJq,
    rzJq,
    sxJq,
    syJq,
    szJq,
    txJq,
    tyJq,
    tzJq,
    Jq;

  //printf("Nvgeo = %d, rows of Vq = %d\n",Nvgeo,Vq.rows());
  //  cout << "Vgeo = " << vgeo << endl;

  // interp to face values 
  MatrixXd Vf   = mesh->Vf;
  MatrixXd rxJf = Vf*(mesh->rxJ);
  MatrixXd sxJf = Vf*(mesh->sxJ);
  MatrixXd txJf = Vf*(mesh->txJ);  
  MatrixXd ryJf = Vf*(mesh->ryJ);
  MatrixXd syJf = Vf*(mesh->syJ);
  MatrixXd tyJf = Vf*(mesh->tyJ);  
  MatrixXd rzJf = Vf*(mesh->rzJ);
  MatrixXd szJf = Vf*(mesh->szJ);
  MatrixXd tzJf = Vf*(mesh->tzJ);  
  MatrixXd Jf   = Vf*(mesh->J); // this may not be accurate. I don't think I use though.
  MatrixXd vfgeo(Nvgeo*(Vf.rows()),K);
  vfgeo << rxJf,
    ryJf,
    rzJf,
    sxJf,
    syJf,
    szJf,
    txJf,
    tyJf,
    tzJf,
    Jf;	  

  // normals computed at face quad points already
  MatrixXd fgeo(Nfgeo*NfpNfaces,K);
  fgeo << mesh->nxJ,
    mesh->nyJ,
    mesh->nzJ,    
    mesh->sJ;
   //cout << "I am Here"
  //printf("fgeo size = %d\n",fgeo.rows());
  //cout << "fgeo = " << fgeo << endl;

  // init rhs, res, Qf to zero
  int Nfields = mesh->Nfields;
  MatrixXd rhs(Nfields*Np,K), res(Nfields*Np,K);
  rhs.fill(0.0);
  res.fill(0.0);
  MatrixXd Qf(Nfields*NfpNfaces,K),rhsf(Nfields*NfpNfaces,K);
  Qf.fill(0.0);
  rhsf.fill(0.0);

#if 0
  printf("moving data to GPU: rhs arry is size %d, %d\n",rhs.rows(),rhs.cols());
  printf("moving data to GPU: rhsf arry is size %d, %d\n",rhsf.rows(),rhsf.cols());
  printf("moving data to GPU: vgeo arry is size %d, %d\n",vgeo.rows(),vgeo.cols());
  printf("moving data to GPU: vfgeo arry is size %d, %d\n",vfgeo.rows(),vfgeo.cols());
  printf("moving data to GPU: fgeo arry is size %d, %d\n",fgeo.rows(),fgeo.cols());
#endif  
  setOccaArray(app, rhs, app->o_rhs);
  setOccaArray(app, res, app->o_res);  
  setOccaArray(app, Qf,  app->o_Qf);
  setOccaArray(app, rhsf,  app->o_rhsf);
  setOccaArray(app, vgeo, app->o_vgeo);
  setOccaArray(app, vfgeo, app->o_vfgeo);
  setOccaArray(app, fgeo, app->o_fgeo);

  // correct for Nfields > 1
  MatrixXi mapPqNfields(NfpNfaces,K);
  for (long long int i = 0; i < NfpNfaces*K; ++i){
    int idP = mesh->mapPq(i);
    int e = idP / NfpNfaces;
    int fidP = idP % NfpNfaces;
    mapPqNfields(i) = fidP + e*NfpNfaces*Nfields;
  }
  setOccaIntArray(app, mapPqNfields, app->o_mapPq);

  setOccaArray(app, mesh->D1D, app->o_D1D); 
  setOccaArray(app, mesh->Vf1D.row(0), app->o_Vf1D); // Vf1D rows 1,2 = mirror images
  setOccaArray(app, mesh->Lf1D.col(0), app->o_Lf1D); // Lf1D cols 1,2 = mirror images    

}


MatrixXd WaveSetU0(Mesh *mesh, int field,int Np,  double(*uexptr)(double, double)){
        
        MatrixXd Q;
	Q.resize(mesh->Nfields*Np,mesh->K);
	Q.fill(0.0);
        int n , k;
        int istart = (field-1)*Np;
	int iend = istart+ Np;
	for(n=istart; n < iend; n++){
	   for(k=0; k<mesh->K; k++){
	       double x=mesh->x(n,k);
	       double y=mesh->y(n,k);
	        Q(n,k)=(*uexptr)(x,y);
	   
	   }
	
	
	}
     return Q;

}




// set occa array:: cast to dfloat
void setOccaArray(App *app, MatrixXd A, occa::memory &c_A){
  int r = A.rows();
  int c = A.cols();
  dfloat *f_A = (dfloat*)malloc(r*c*sizeof(dfloat));
  Map<MatrixXdf >(f_A,r,c) = A.cast<dfloat>();
  c_A = app->device.malloc(r*c*sizeof(dfloat),f_A);
  free(f_A);  
}

// set occa array for int matrices
void setOccaIntArray(App *app, MatrixXi A, occa::memory &c_A){
  int r = A.rows();
  int c = A.cols();
  int *f_A = (int*)malloc(r*c*sizeof(int));
  Map<MatrixXi >(f_A,r,c) = A;
  c_A = app->device.malloc(r*c*sizeof(int),f_A);
  free(f_A);
}

// get occa array:: cast to double
void getOccaArray(App *app, occa::memory c_A, MatrixXd &A){
  int r = A.rows();
  int c = A.cols();
  dfloat *f_A = (dfloat*)malloc(r*c*sizeof(dfloat));
  c_A.copyTo(f_A);
  for (int i = 0; i < r*c; ++i){
    A(i) = f_A[i]; // somewhat inefficient
 }

  free(f_A);
}
		 
void setOccaIntVector(App *app, VectorXi A, occa::memory &c_A){
  int c = A.size();
  int *f_A = (int*)malloc(c*sizeof(int));
  Map<VectorXi >(f_A,c) = A;
  c_A = app->device.malloc(c*sizeof(int),f_A);
  free(f_A);
}


