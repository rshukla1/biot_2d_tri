syms c11 c12 c13 real;
syms c33 real;
syms c55 real ;
syms a1 a3 real;
syms M real;
syms  m1 m3 real;
syms  rho rhof real;




inC=[c33/(c11*c33-c13*c13) -c13/(c11*c33-c13*c13) 0 (a1*c33 - a3*c13)/(c11*c33-c13*c13);
    -c13/(c11*c33-c13*c13)  c11/(c11*c33-c13*c13) 0  (a3*c11 - a1*c13)/(c11*c33-c13*c13);
    0 0 1/c55 0;
    (a1*c33 - a3*c13)/(c11*c33-c13*c13) (a3*c11 - a1*c13)/(c11*c33-c13*c13) 0 (1/M) + (a1*a1*c33 + a3*a3*c11 -2*a1*a3*c13)/(c11*c33-c13*c13)
 
];



Es=[(1/M) + (a1*a1*c33 + a3*a3*c11 -2*a1*a3*c13)/(c11*c33-c13*c13) (a1*c33 - a3*c13)/(c11*c33-c13*c13) (a3*c11 - a1*c13)/(c11*c33-c13*c13) 0;
    (a1*c33 - a3*c13)/(c11*c33-c13*c13) c33/(c11*c33-c13*c13) -c13/(c11*c33-c13*c13) 0;
    (a3*c11 - a1*c13)/(c11*c33-c13*c13) -c13/(c11*c33-c13*c13)  c11/(c11*c33-c13*c13) 0 ;
    0 0 0 1/c55;
    
    ];

 Ev=[rho 0 rhof 0 ;
     0 rho 0 rhof ;
     rhof 0 m1 0 ;
     0 rhof 0 m3;
];

 Q=blkdiag(Es,Ev);
 inQ=inv(Q)
