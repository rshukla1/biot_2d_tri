#include "fem.h"
double GaussPulse(double x, double y){

  double r = (x*x + y*y);
  double a=10;
  return exp(-a*r*r);
}

int main(int argc, char **argv){

  //occa::printModeInfo();   //return 0;
  int N =4;
  int K1D =80;

  //printf("argc = %d\n",argc);
 Mesh *mesh = new Mesh;
 
#if 1  
  if (argc > 2){
    N = atoi(argv[1]);
    K1D = atoi(argv[2]);
    printf("setting N = %d, K1D = %d\n",N,K1D);
  }
#endif

#if 0  
  if (argc > 2){
    N = atoi(argv[1]);
    ReadGmesh2d(mesh, argv[2]);
}
#endif

  

  double FinalTime = 100;
  double CFL = 0.2;  

  if (argc > 4){
    CFL = atof(argv[3]);
    FinalTime = atof(argv[4]);
    printf("setting CFL = %f, T = %f, curved warping a =%f\n",CFL,FinalTime);
  }



   TriMesh2d(mesh,K1D,K1D); // make Cartesian mesh
  
  // ============ physics independent stuff ===========

  InitRefDataTri2d(mesh, N);
  MapNodes2d(mesh); // low order mapping

  GeometricFactors2d(mesh); 
  Normals2d(mesh); 

  int dim = 2;
  ConnectElems(mesh,dim);

  // change Vf
  MatrixXd xf = (mesh->Vf)*(mesh->x);
  MatrixXd yf = (mesh->Vf)*(mesh->y);
  MatrixXd zf(xf.rows(),xf.cols()); zf.fill(0.0);

 MatrixXi mapPq;
 BuildFaceNodeMaps(mesh,xf,yf,zf,mapPq);

 double DX = mesh->VX.maxCoeff()-mesh->VX.minCoeff();
 double DY = mesh->VY.maxCoeff()-mesh->VY.minCoeff();
 //MakeNodeMapsPeriodic2d(mesh,xf,yf,DX,DY,mapPq);
 // mapPq.fill(0.0);
  mesh->mapPq = mapPq;
  BuildMaps2D(mesh);

  // ============ problem dependent stuff ============

  int Np = mesh->Np;
  int NfpNfaces = mesh->Nfp * mesh->Nfaces;  
  int K = mesh->K;
  
  int Nfields = 8; 
  mesh->Nfields = Nfields;
  App *app = new App;
  //app->device.setup("mode: 'Serial'");
  app->device.setup("mode: 'CUDA', device_id: 0");  
  //app->device.setup("mode: 'OpenMP', threads: 4"); 
  
  //app->device.setup("mode: 'OpenCL', device_id: 0, platform_id: 0");
  setupOccaMesh2d(mesh,app); // build mesh geofacs
  app->props["defines/p_Nfields"] = Nfields;
  if (sizeof(dfloat)==4){
    app->props["defines/p_tau_v"] = 1.f;
    app->props["defines/p_tau_s"] = 1.f;

  }else{
    app->props["defines/p_tau_s"] = 1.f;
    app->props["defines/p_tau_v"] = 1.f;
  }

  //app->props["defines/p_tau_s"] = 0.0;
  //app->props["defines/p_tau_v"] = 0.0;
  
  
  // build occa kernels  
  string path = "okl/PoroElasKernelWADG.okl";
  
  app->volume = app->device.buildKernel(path.c_str(),"rk_volume_elas",app->props);
  app->surface = app->device.buildKernel(path.c_str(),"rk_surface_elas",app->props);
  app->update = app->device.buildKernel(path.c_str(),"rk_update_elas",app->props);
  
  double time = 0.0;
  
  
  //double (*uexptr)(double, double)=NULL;
  //uexptr = &GaussPulse;
  //MatrixXd Q=WaveSetU0(mesh,1,Np,uexptr);
  /*FILE *fp;

    int k, n;

    int iField=1;
    fp=fopen("./PostProc/outPut.dat", "w");
  
    int istart=(iField-1)*Np;
    int iend=istart+Np;
  
    for(n=istart; n < iend; n++){
    for(k=0;k < K; k++){
    fprintf(fp, "%16.15f ", Q(n,k) );

    }
    fprintf(fp, "\n");
    }
  
    fclose(fp);*/


  MatrixXd Q(Np*Nfields, K);
  Q.fill(0.0); 
 
/* 
    for(int i=0; i < Nfields; i++){
    for (int j = 0; j < Np; ++j){
    Q(j+i*Np,0)=(double)j + 1;
    Q(j+i*Np,1)=(double)j + 2;
    }
    }
*/   
  setOccaArray(app, Q, app->o_Q);

 
  // ============== run RK solver ==================

  double h = mesh->sJ.maxCoeff(); 
  double CN = (N+1)*(N+1)/2.0; 
  double CNh = CN*h;
  double dt = CFL * 2.0/ CNh;
  int Nsteps = (int) ceil(FinalTime/dt);
  dt = FinalTime/(double) Nsteps;

  printf("dt = %f, FinalTime = %f, Nsteps = %d\n",dt,FinalTime,Nsteps);  

  int interval = max((int) ceil(Nsteps/10),1);  
  printf("Interval = %d\n",interval);

#if 0
   int tstep = 10;
   clock_t begin;
   clock_t end;
   double elapsed_sec_volume  = 0.0; 
   double elapsed_sec_surface = 0.0;
   double elapsed_sec_update  = 0.0;;


   for (int step = 0; step < tstep; ++step){
    
    begin = clock();
    app->volume(K, app->o_vgeo, app->o_Dr, app->o_Ds,app->o_Q, app->o_rhs);
    app->device.finish();
    end = clock();
    elapsed_sec_volume += pow(10,9)*double(end-begin)/(CLOCKS_PER_SEC);
   
    
    
    begin = clock();
    app->surface(K, app->o_fgeo, app-> o_Fmask, app->o_vmapP, app->o_LIFT, app->o_Q, app->o_rhs); 
    app->device.finish();
    end = clock();
    elapsed_sec_surface += pow(10,9)*double(end-begin)/(CLOCKS_PER_SEC);
 

   begin = clock();
   app->update(K, app->o_Vq, app->o_Pq, app->o_rho, app->o_rhof, app->o_c11, app->o_c12, app->o_c13, app->o_c33,app->o_c55, app->o_a1, app->o_a3,app->o_m1, app->o_m3, app->o_M, app->o_eta, app->o_kappa1, app->o_kappa3, 1.0, app->o_fsrcq, 1.0, 1.0, 1.0, app->o_rhs, app->o_res, app->o_Q);
   app->device.finish();
   end = clock();
   elapsed_sec_update += pow(10,9)*double(end-begin)/(CLOCKS_PER_SEC);
 
  }
   cout <<"vol time= "      << elapsed_sec_volume/10.0  <<  endl;
   cout <<"surface time= "  << elapsed_sec_surface/10.0 <<  endl;
   cout <<"update time= "   << elapsed_sec_update/10.0  <<  endl;





   
  return 0; 
#endif



  int NINT = mesh->rk4a.size();
  for (int i = 0; i < Nsteps; ++i){
    //Ricker
    dfloat f0=3730.0*pow(10.0,-6);
    dfloat tR = 1.0/f0;
    dfloat at = M_PI *f0*(time-tR);
    dfloat ftime=(1.0 - 2.0*at*at)*exp(-at*at);

    for (int INTRK = 0; INTRK < NINT; ++INTRK){

      const dfloat fdt = (dfloat) dt;
      const dfloat fa  = (dfloat) mesh->rk4a[INTRK];
      const dfloat fb  = (dfloat) mesh->rk4b[INTRK];
      
      app->volume(K, app->o_vgeo, app->o_Dr, app->o_Ds,app->o_Q, app->o_rhs);
      app->surface(K, app->o_fgeo, app-> o_Fmask, app->o_vmapP, app->o_LIFT, app->o_Q, app->o_rhs); 
      app->update(K, app->o_Vq, app->o_Pq, app->o_rho, app->o_rhof, app->o_c11, app->o_c12, app->o_c13, app->o_c33,app->o_c55, app->o_a1, app->o_a3,app->o_m1, app->o_m3, app->o_M, app->o_eta, app->o_kappa1, app->o_kappa3, ftime, app->o_fsrcq, fa, fb, fdt,app->o_rhs, app->o_res, app->o_Q);

    }

    time+=dt;

    if (i % interval == 0){
      printf("on timestep %d out of %d\n",i+1,Nsteps);
    }
  }

  getOccaArray(app,app->o_Q,Q);
  
  
  FILE *fp;

  int k, n;

  int iField=5;
  fp=fopen("./PostProc/outPut.dat", "w");
  
  int istart=(iField-1)*Np;
  int iend=istart+Np;
  
  for(n=istart; n < iend; n++){
    for(k=0;k < K; k++){
      fprintf(fp, "%16.15f ", Q(n,k) );

    }
    fprintf(fp, "\n");
  }
  
  fclose(fp);


 
 
  return 0;  
}
