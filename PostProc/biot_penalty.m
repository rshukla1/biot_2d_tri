% function Biot2D
 
clear all -globals
Globals2D
N =4;


[Nv, VX, VY, K, EToV] = MeshReaderGmsh2D("../meshes/Lshape2.msh");
VX=VX*2000;
VY=VY*2000;
StartUp2D;
data=load('outPut.dat');

disp('Writing in VTK');
WriteVTK2D(strcat(strcat('vgpuv1'), '.vtk'), N, {'bx'},data);


return

 
Q=zeros(Np,K);
%Q=exp(-0.001*(x.^2+y.^2));
%PlotField2D(N,x,y,Q)
data=load('outPut.dat');
PlotField2D(N,x,y,data);
view(2);








%BuildPeriodicMaps2D(2,2);
 
% plotting nodes
[rp sp] = EquiNodes2D(50); [rp sp] = xytors(rp,sp);
Vp = Vandermonde2D(N,rp,sp)/V;
xp = Vp*x; yp = Vp*y;
 
Nq = 2*N+1;
[rq sq wq] = Cubature2D(Nq); % integrate u*v*c
Vq = Vandermonde2D(N,rq,sq)/V;
Pq = V*V'*Vq'*diag(wq); % J's cancel out
Mref = inv(V*V');
xq = Vq*x; yq = Vq*y;
Jq = Vq*J;
 return
 
%%%%% params setup
 
U = cell(8,1);
for i = 1:8
    U{i} = zeros(Np,K);
end
% U{1} = exp(-20^2*(x.^2+y.^2));
% U{3} = exp(-20^2*(x.^2+y.^2));
% U{5} = exp(-10^2*(x.^2+y.^2));
 
% randomly chosen Es, Ev
% Es = [ones(3)+2*eye(3) zeros(3,1)
%     zeros(1,3) 2];
% Ev = [2 0 -1 0 ;0 2 0 -1; -1 0 2 0; 0 -1 0 2];
x0 = eps;
y0 = eps;
[vals,p] = sort(abs(x(:) - x0)+abs(y(:)-y0));
ids = abs(vals)<1e-8; ids = p(ids);
ids;
%return
global rick ptsrc 
f0 = 3135*(10^-6); tR = 1/f0;

rick = @(t) (1 - 2*(pi*f0*(t-tR)).^2).*exp(-(pi*f0*(t-tR)).^2);

ptsrc = zeros(Np,K); 
ptsrc(ids) = 1;
ptsrc = (V*V')*ptsrc;
[Q]=q_matrix();
Es=Q(1:4, 1:4);
Ev=Q(5:8, 5:8);
A0 =blkdiag(Es,Ev);
global invA0
invA0 = inv(A0);
 
 
%%%%%%
 
time = 0;
 
% Runge-Kutta residual storage
res = cell(8,1);
for fld = 1:8
    res{fld} = zeros(Np,K);
end
 
% compute time step size
CN = (N+1)^2/2; % guessing...
%dt = 1/(CN*max(abs(sJ(:)))*max(abs(1./J(:))));
CNh = max(CN*max(Fscale(:)));
dt = CFL*2/CNh;
LS1=-(ETA./KAPPA1).*BETA22X
LS3=-(ETA./KAPPA3).*BETA22Z;
% outer time step loop
% colormap(gray)fc=3730;
tstep=0;
Ntsteps =ceil(FinalTime/dt)
while (time<FinalTime)
    tstep=tstep+1
    if(time+dt>FinalTime), dt = FinalTime-time; end
    U{5}=U{5}+(BETA12X/BETA22X).*(exp(LS1.*dt)-1).*U{7};  
    U{6}=U{6}+(BETA12Z/BETA22Z).*(exp(LS3.*dt)-1).*U{8};
    U{7}=exp(LS1*dt)*U{7};
    U{8}=exp(LS3*dt)*U{8};

    for INTRK = 1:5
        
        timelocal = time + rk4c(INTRK)*dt;
        
        rhs = RHS2D(U,timelocal);
        
        % initiate and increment Runge-Kutta residuals
        for fld = 1:length(U)
            res{fld} = rk4a(INTRK)*res{fld} + dt*rhs{fld};
            U{fld} = U{fld} + rk4b(INTRK) * res{fld};
        end                
        
    end
%     
    if 0 
        clf
          pp = U{5};
          vv = Vp*pp;
        %         vv = abs(vv);
        color_line3(xp,yp,vv,vv,'.');
        axis equal
        axis tight
        colorbar
%         caxis([-.1 .2])
%         axis([0 1 0 1 -10 10])
        %         PlotField2D(N+1, x, y, pp); view(2)
        title(sprintf('time = %f',time))
        drawnow
    end
%  f=exp(-0.5*fc*fc*(time-t0)^2)*cos(pi*fc*(time-t0));
%  [ktria,GX,GY]=sample(0,0);
%  srcterm=Sample2DDirac(0,0);
%      for i=1:1:Np
%              U{3}(i,ktria)=U{3}(i,ktria) +  10^-2*srcterm(i)*f;
%              U{1}(i,ktria)=U{1}(i,ktria) + 10^-2*srcterm(i)*f;
%      end
   % Increment time
   %U{7}=exp(-ETA*0.5/KAPPA1)*U{7};
   %U{8}=exp(-ETA*0.5/KAPPA3)*U{8};
   time = time+dt; 
   
%    if(mod(tstep,200)==0)
%        fprintf('At time step %d the max value of v1 is %5.6f\n', tstep, max(max(U{5})));
%    end
%     if((mod(tstep,10)==0))
%         bx=U{5} + (RHOF/RHO)*U{7};
%         bz=U{6} + (RHOF/RHO)*U{8};
%        WriteVTK2D(strcat(strcat('bx_rev',num2str(tstep)), '.vtk'), N, {'bx'},bx);
%        WriteVTK2D(strcat(strcat('bz_rev',num2str(tstep)), '.vtk'), N, {'bz'},bz);
%     
%   
%    end
end
 
% axis off
% view(0,0)
 
%  
bx=U{5} + (RHOF/RHO)*U{7};
bz=U{6} + (RHOF/RHO)*U{8};
WriteVTK2D(strcat(strcat('bx_re_visicd',num2str(tstep)), '.vtk'), N, {'bx'},bx);
WriteVTK2D(strcat(strcat('bz_re_viscid',num2str(tstep)), '.vtk'), N, {'bz'},bz);
